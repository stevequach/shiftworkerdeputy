//
//  Constant.swift
//  Shiftworker
//
//  Created by Nghia Quach on 23/2/17.
//  Copyright © 2017 Nghia Quach. All rights reserved.
//

import Foundation

struct GlobalConstants {
   
   //MARK: Deputy WS API
   enum DeputyWSPath:String{
      case StartShift = "shift/start"
      case EndShift = "shift/end"
      case ListShift = "shifts"
   }
   
   enum DeputyWSFields:String{
      case id = "id"
      case start = "start"
      case end = "end"
      case startLatitude = "startLatitude"
      case startLongitude = "startLongitude"
      case endLongitude = "endLongitude"
      case endLatitude = "endLatitude"
      case image = "image"
   }
   
   enum HttpMethod:String {
      case Post = "Post"
      case Get = "Get"
   }
   
   enum ShiftStatus:String {
      case InProgress = "In Progress"
      case Finish = "Finish"
   }
   
   // Constant variables for http connection
   static let BasedURL = "https://apjoqdqpi3.execute-api.us-west-2.amazonaws.com/dmc/"
   static let LoginString = "Deputy NghiaQuach";
   static let AuthorizarionField = "Authorization"

}
