//
//  ShiftDetailsView.swift
//  Shiftworker
//
//  Created by Nghia Quach on 25/2/17.
//  Copyright © 2017 Nghia Quach. All rights reserved.
//

import UIKit
import MapKit

class ShiftDetailsView : UIViewController, CLLocationManagerDelegate, MKMapViewDelegate{
   
   @IBOutlet weak var detailsView: UIView!
   @IBOutlet weak var map: MKMapView!
   @IBOutlet weak var startShiftLabel: UILabel!
   @IBOutlet weak var statusLabel: UILabel!
   @IBOutlet weak var endShiftLabel: UILabel!
   
   var shiftDict = NSDictionary()
   
   var locationManager: CLLocationManager = CLLocationManager()
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.loadShiftDetail()
      self.loadMapWithLocations()
   }
   
   //load shift data from previous table view and set text for table
   func loadShiftDetail(){
      let startShiftDate = shiftDict.object(forKey: GlobalConstants.DeputyWSFields.start.rawValue) as! String?
      let endShiftDate = shiftDict.object(forKey: GlobalConstants.DeputyWSFields.end.rawValue) as! String?
      
      startShiftLabel.text = formatDateString(sysDateString: startShiftDate!)
      endShiftLabel.text = formatDateString(sysDateString: endShiftDate!)
      
      if (!(startShiftDate?.isEmpty)! && (endShiftDate?.isEmpty)!){
         statusLabel.textColor = UIColor.red
         statusLabel.text = GlobalConstants.ShiftStatus.InProgress.rawValue
      }
      else{
         statusLabel.textColor = UIColor.blue
         statusLabel.text = GlobalConstants.ShiftStatus.Finish.rawValue
      }
   }
   
   //MARK: MAPKIT
   //display map with source and destination locations
   func loadMapWithLocations(){
      map.showsUserLocation = true
      
      let startLatitude = Double((shiftDict.object(forKey: GlobalConstants.DeputyWSFields.startLatitude.rawValue) as! String?)!)
      let startLongitude = Double(shiftDict.object(forKey: GlobalConstants.DeputyWSFields.startLongitude.rawValue) as! String)
      
      let endLatitude = Double((shiftDict.object(forKey: GlobalConstants.DeputyWSFields.endLatitude.rawValue) as! String?)!)
   
      let endLongitude = Double((shiftDict.object(forKey: GlobalConstants.DeputyWSFields.endLongitude.rawValue) as! String?)!)
      
      map.delegate = self
      
      // setting the source and destination location
      let sourceLocation = CLLocationCoordinate2D(latitude: startLatitude!, longitude: startLongitude!)
      let destinationLocation = CLLocationCoordinate2D(latitude: endLatitude!, longitude: endLongitude!)
      
      // set the marks for source and destination
      let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
      let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
      let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
      let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
      
      // set source and destincation annotation
      let sourceAnnotation = MKPointAnnotation()
      sourceAnnotation.title = "Start Location"
      
      if let location = sourcePlacemark.location {
         sourceAnnotation.coordinate = location.coordinate
      }
      
      let destinationAnnotation = MKPointAnnotation()
      destinationAnnotation.title = "End Location"
      
      if let location = destinationPlacemark.location {
         destinationAnnotation.coordinate = location.coordinate
      }
      self.map.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
      
      // set the direction
      let directionRequest = MKDirectionsRequest()
      directionRequest.source = sourceMapItem
      directionRequest.destination = destinationMapItem
      directionRequest.transportType = .automobile
      
      // Calculate the direction
      let directions = MKDirections(request: directionRequest)
      directions.calculate {
         (response, error) -> Void in
         
         guard let response = response else {
            if let error = error {
               print("Error: \(error)")
            }
            return
         }
         
         let route = response.routes[0]
         self.map.add((route.polyline), level: MKOverlayLevel.aboveRoads)
         
         let rect = route.polyline.boundingMapRect
         self.map.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
      }
   }
   
   func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
      let lastLocation: CLLocation = locations[locations.count - 1]
      
      animateMap(lastLocation)
   }
   
   func animateMap(_ location: CLLocation) {
      let region = MKCoordinateRegionMakeWithDistance(location.coordinate, 1000, 1000)
      map.setRegion(region, animated: true)
   }
   
   func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
      let renderer = MKPolylineRenderer(overlay: overlay)
      renderer.strokeColor = UIColor.red
      renderer.lineWidth = 4.0
      
      return renderer
   }
   
}
