//
//  ShiftTableViewCell.swift
//  Shiftworker
//
//  Created by Nghia Quach on 24/2/17.
//  Copyright © 2017 Nghia Quach. All rights reserved.
//

import UIKit

class ShiftTableViewCell: UITableViewCell {
   
   //MARK: Properties
   @IBOutlet weak var photoImageView: UIImageView!
   @IBOutlet weak var startDateLabel: UILabel!
   @IBOutlet weak var endDateLabel: UILabel!
   @IBOutlet weak var statusLabel: UILabel!
   
   
   override func awakeFromNib() {
      super.awakeFromNib()
      // Initialization code
   }
   
   override func setSelected(_ selected: Bool, animated: Bool) {
      super.setSelected(selected, animated: animated)
   }
   
}
