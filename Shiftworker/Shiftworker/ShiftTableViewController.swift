//
//  ViewController.swift
//  Shiftworker
//
//  Created by Nghia Quach on 21/2/17.
//  Copyright © 2017 Nghia Quach. All rights reserved.
//

import UIKit
import os.log

class ShiftTableViewController: UITableViewController {
   
   //Create Activity Indicator
   var myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
   
   enum ShiftLabel:String {
      case StartShiftLabel = "Start Shift: "
      case EndShiftLabel = "End Shift: "
   }
   
   var shifts = NSArray()
   var isInprogress = false;
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(true)
      //Checking internet connection
      //Get the previous shift data
      if (isConnectedToNetwork()){
         self.loadOnlineShifts()
      }
      else{
         //disable start and end functions if there is no internet connection
         self.navigationItem.leftBarButtonItem?.isEnabled = false;
         self.navigationItem.rightBarButtonItem?.isEnabled = false;
         //load the offline data
         self.loadOfflineShifts()
      }
   }
   
   override func viewDidLoad() {
      super.viewDidLoad()
      //enable start shift button if user does not have any previous shift
      if(shifts.count==0){
         self.navigationItem.rightBarButtonItem?.isEnabled = true;
      }
   }
   
   //MARK: buttons handler
   //handle action of start shift button
   @IBAction func onStartShiftButton(_ sender: Any) {
      doStartShift()
   }
   
   //handle action of end shift button
   @IBAction func onEndShiftButton(_ sender: Any) {
      doEndShift()
   }
   
   //MARK: Do request and get response from Deputy WS
   //Get current time, latitude and longitude as a body message for the request
   func GetDictDataForHTTPBody() -> Any{
      let currentLocation = GetCurrentLocation()
      
      let strDate = Date().iso8601
      let latitude = "\(currentLocation.coordinate.latitude)"
      let longitude = "\(currentLocation.coordinate.longitude)"
      
      let dict = ["time": strDate, "latitude": latitude,"longitude": longitude]
      
      return dict
   }
   
   // call start shift API and processing the response
   func doStartShift(){
      
      HTTPGetJSON(url: GlobalConstants.BasedURL + GlobalConstants.DeputyWSPath.StartShift.rawValue,  httpMethod:GlobalConstants.HttpMethod.Post.rawValue, data:self.GetDictDataForHTTPBody()){
         (data: AnyObject, error: String?) -> Void in
         
         if error != nil {
            showAlertDialog(view: self, theMessage: error!)
         } else {
            self.loadOnlineShifts()
         }
      }
   }
   
   // call end shift API and processing the response
   func doEndShift(){
      
      HTTPGetJSON(url: GlobalConstants.BasedURL + GlobalConstants.DeputyWSPath.EndShift.rawValue,  httpMethod:GlobalConstants.HttpMethod.Post.rawValue, data:GetDictDataForHTTPBody()){
         (data: AnyObject, error: String?) -> Void in
         
         if error != nil {
            showAlertDialog(view: self, theMessage: error!)
         } else {
            self.loadOnlineShifts()
         }
      }
   }
   
   // call load shift api and processing the response
   func loadOnlineShifts(){
      showActivityIndicator(view: self.view,indicatorView: self.myActivityIndicator)
      
      HTTPGetJSON(url: GlobalConstants.BasedURL + GlobalConstants.DeputyWSPath.ListShift.rawValue,  httpMethod:GlobalConstants.HttpMethod.Get.rawValue, data:nil){
         (data: AnyObject, error: String?) -> Void in
         
         DispatchQueue.main.async{
            if error != nil {
               showAlertDialog(view: self, theMessage: error!)
            } else {
               self.shifts = data as! NSArray
               self.shifts = self.shifts.reversed() as NSArray
               
               //save the shifts data
               self.saveShifts()
               
               self.tableView.reloadData()
            }
            hideActivityIndicator(view: self.view,indicatorView: self.myActivityIndicator)
         }
      }
   }
   
   override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
   }
   
   //MARK: - Table view data source
   
   override func numberOfSections(in tableView: UITableView) -> Int {
      return 1
   }
   
   // number of rows in table view
   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return shifts.count
   }
   
   // create a cell for each table view row
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
      // Table view cells are reused and should be dequeued using a cell identifier.
      let cellIdentifier = "ShiftTableViewCell"
      //
      guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ShiftTableViewCell  else {
         fatalError("The dequeued cell is not an instance of ShiftTableViewCell.")
      }
      
      // Fetches the appropriate shift for the data source layout.
      let shiftDict = shifts[indexPath.row] as! NSDictionary
      let strStartShift = shiftDict.object(forKey: GlobalConstants.DeputyWSFields.start.rawValue) as! String
      let strEndShift = shiftDict.object(forKey: GlobalConstants.DeputyWSFields.end.rawValue) as! String
      let photoLink = shiftDict.object(forKey: GlobalConstants.DeputyWSFields.image.rawValue) as! String
      
      cell.startDateLabel.text = ShiftLabel.StartShiftLabel.rawValue + formatDateString(sysDateString: strStartShift)
      cell.endDateLabel.text = ShiftLabel.EndShiftLabel.rawValue + formatDateString(sysDateString: strEndShift)
      cell.photoImageView.downloadedFrom(link: photoLink)
      
      //check the status of the shift in on the first row
      if (indexPath.row == 0){
         if (strEndShift == ""){
            //if any shift is in progress status, the start button will be disabled
            self.navigationItem.rightBarButtonItem?.isEnabled = false;
            self.navigationItem.leftBarButtonItem?.isEnabled = true;
            
            cell.statusLabel.textColor = UIColor.red
            cell.statusLabel.text = GlobalConstants.ShiftStatus.InProgress.rawValue
            isInprogress = true
         }
         else{
            self.navigationItem.rightBarButtonItem?.isEnabled = true;
            self.navigationItem.leftBarButtonItem?.isEnabled = false;
            
            cell.statusLabel.textColor = UIColor.blue
            cell.statusLabel.text = GlobalConstants.ShiftStatus.Finish.rawValue
            isInprogress = false
         }
      }
      else{
         cell.statusLabel.textColor = UIColor.blue
         cell.statusLabel.text = GlobalConstants.ShiftStatus.Finish.rawValue
      }
      
      return cell
   }
   
   // method to run when table view cell is tapped
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
      let theShiftDict = shifts[indexPath.row] as? NSDictionary
      
      let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShiftDetailsView") as! ShiftDetailsView
      vc.shiftDict = theShiftDict!
      vc.title = "Shift Details"
      
      self.navigationController?.pushViewController(vc, animated: true)
   }
   
   //store all shift data to local database
   private func saveShifts() {
      let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(shifts, toFile: Shift.ArchiveURL.path)
      if isSuccessfulSave {
         os_log("Shifts successfully saved.", log: OSLog.default, type: .debug)
      } else {
         os_log("Failed to save shifts...", log: OSLog.default, type: .error)
      }
   }
   
   //load the offline data
   private func loadOfflineShifts() {
      self.shifts = (NSKeyedUnarchiver.unarchiveObject(withFile: Shift.ArchiveURL.path) as? NSArray)!
   }
}

