//
//  Shift.swift
//  Shiftworker
//
//  Created by Nghia Quach on 22/2/17.
//  Copyright © 2017 Nghia Quach. All rights reserved.
//

import UIKit
import os.log


class Shift: NSObject, NSCoding {
   
   //MARK: Archiving Paths
   static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
   static let ArchiveURL = DocumentsDirectory.appendingPathComponent("shifts")
   
   
   //MARK: Properties
   var id: String
   var start: String
   var end: String
   var startLatitude: String
   var startLongitude: String
   var endLatitude: String
   var endLongitude: String
   var image: UIImage?
   
   struct PropertyKey{
      static let id = "id"
      static let start = "start"
      static let end = "end"
      static let startLatitude = "startLatitude"
      static let startLongitude = "startLongitude"
      static let endLatitude = "endLatitude"
      static let endLongitude = "endLongitude"
      static let image = "image"
   }
   
   override init() {
      self.id = ""
      self.start = ""
      self.end = ""
      self.startLatitude = ""
      self.startLongitude = ""
      self.endLatitude = ""
      self.endLongitude = ""
      self.image = nil

   }
   
   init?(id: String, start: String, end: String,startLatitude:String,startLongitude:String
      , endLatitude: String, endLongitude: String, image:UIImage?) {
      
      self.id = id
      self.start = start
      self.end = end
      self.startLatitude = startLatitude
      self.startLongitude = startLongitude
      self.endLatitude = endLatitude
      self.endLongitude = endLongitude
      self.image = image
   }
   
   //encode properties to dictionary
   func encode(with aCoder: NSCoder) {
      aCoder.encode(id, forKey: PropertyKey.id)
      aCoder.encode(start, forKey: PropertyKey.start)
      aCoder.encode(end, forKey: PropertyKey.end)
      aCoder.encode(startLatitude, forKey: PropertyKey.startLatitude)
      aCoder.encode(startLongitude, forKey: PropertyKey.startLongitude)
      aCoder.encode(endLatitude, forKey: PropertyKey.endLatitude)
      aCoder.encode(endLongitude, forKey: PropertyKey.endLongitude)
      aCoder.encode(image, forKey: PropertyKey.image)
   }
   
   required convenience init?(coder aDecoder: NSCoder) {
      
      // The id is required. If we cannot decode a id string, the initializer should fail.
      guard let id = aDecoder.decodeObject(forKey: PropertyKey.id) as? String else {
         os_log("Unable to decode the id for a Shift object.", log: OSLog.default, type: .debug)
         return nil
      }
      
      // Because photo is an optional property of Shift, just use conditional cast.
      let start = aDecoder.decodeObject(forKey: PropertyKey.start)
      let end = aDecoder.decodeObject(forKey: PropertyKey.end)
      let startLatitude = aDecoder.decodeObject(forKey: PropertyKey.startLatitude)
      let startLongitude = aDecoder.decodeObject(forKey: PropertyKey.startLongitude)
      let endLatitude = aDecoder.decodeObject(forKey: PropertyKey.endLatitude)
      let endLongitude = aDecoder.decodeObject(forKey: PropertyKey.endLongitude)
      let image = aDecoder.decodeObject(forKey: PropertyKey.image) as! UIImage
      
      // Must call designated initializer.
      self.init(id: id, start: start as! String, end: end as! String,startLatitude: startLatitude as! String,startLongitude: startLongitude as! String, endLatitude: endLatitude as! String, endLongitude: endLongitude as! String , image:image)
   }
}


