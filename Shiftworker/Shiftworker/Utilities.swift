//
//  Utilities.swift
//  Shiftworker
//
//  Created by Nghia Quach on 25/2/17.
//  Copyright © 2017 Nghia Quach. All rights reserved.
//

import CoreLocation
import UIKit
import SystemConfiguration

//download images from the URL and store it in a UIImageView
extension UIImageView {
   func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
      contentMode = mode
      URLSession.shared.dataTask(with: url) { (data, response, error) in
         guard
            let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
            let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
            let data = data, error == nil,
            let image = UIImage(data: data)
            else { return }
         DispatchQueue.main.async() { () -> Void in
            self.image = image
         }
         }.resume()
   }
   
   func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
      guard let url = URL(string: link) else { return }
      downloadedFrom(url: url, contentMode: mode)
   }
}

//format current date to iso8601
extension Date {
   static let iso8601Formatter: DateFormatter = {
      let formatter = DateFormatter()
      formatter.calendar = Calendar(identifier: .iso8601)
      formatter.locale = Locale(identifier: "en_US_POSIX")
      formatter.timeZone = TimeZone(secondsFromGMT: 0)
      formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
      return formatter
   }()
   
   var iso8601: String {
      return Date.iso8601Formatter.string(from: self)
   }
}

//covert a iso8601 string to human readable date
func formatDateString (sysDateString: String)->String {
   
   if(!sysDateString.isEmpty){
   
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
      dateFormatter.locale = Locale(identifier: "en_US_POSIX")
      
      let dateObj = dateFormatter.date(from: sysDateString)
      dateFormatter.dateFormat = "dd MMM yyyy HH:mm:ss"
      
      return dateFormatter.string(from: dateObj!)
   }
   return sysDateString
}

extension String {
   var dateFromISO8601: Date? {
      return Date.iso8601Formatter.date(from: self)
   }
}

//get a current device location
func GetCurrentLocation() -> CLLocation{
   let locManager = CLLocationManager()
   locManager.requestWhenInUseAuthorization()
   
   var currentLocation = CLLocation()
   
   if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
      CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
      
      currentLocation = locManager.location!
      
   }
   return currentLocation
}

//checking internet connection available
func isConnectedToNetwork() -> Bool {
   
   var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
   zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
   zeroAddress.sin_family = sa_family_t(AF_INET)
   let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
      $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
         SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
      }
   }
   
   var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
   if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
      return false
   }
   
   let isReachable = flags == .reachable
   let needsConnection = flags == .connectionRequired
   
   return isReachable && !needsConnection
}

//show alert dialog
func showAlertDialog(view: UIViewController,theMessage: String){
   let alert = UIAlertController(title: "Alert", message: theMessage, preferredStyle: UIAlertControllerStyle.alert)
   alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
   view.present(alert, animated: true, completion: nil)
}

//show activity indicator
func showActivityIndicator(view: UIView, indicatorView:UIActivityIndicatorView){
  
   view.isUserInteractionEnabled = false
   // Position Activity Indicator in the center of the main view
   indicatorView.center = view.center
   indicatorView.hidesWhenStopped=true
   view.addSubview(indicatorView)
   indicatorView.startAnimating()
}

//show activity indicator
func hideActivityIndicator(view: UIView, indicatorView:UIActivityIndicatorView){
   view.isUserInteractionEnabled = true
   indicatorView.isHidden = true
   indicatorView.stopAnimating()
}
