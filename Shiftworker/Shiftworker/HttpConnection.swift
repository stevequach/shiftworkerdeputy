//
//  HttpConnection.swift
//  Shiftworker
//
//  Created by Nghia Quach on 22/2/17.
//  Copyright © 2017 Nghia Quach. All rights reserved.
//

import Foundation
import UIKit


//Send a http request return a callback
func HTTPsendRequest(request: NSMutableURLRequest,
                     callback: @escaping (String, String?) -> Void) {
   
   let task = URLSession.shared.dataTask(
      with: request as URLRequest, completionHandler :
      {
         data, response, error in
         if error != nil {
            callback("", (error!.localizedDescription) as String)
         } else {
            callback(
               NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String,
               nil
            )
         }
   })
   task.resume()
}

//Get JSON Data from Server return a callback
func HTTPGetJSON(
   url: String,
   httpMethod: String,
   data: Any?,
   callback: @escaping (AnyObject, String?) -> Void) {
   
   let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
   request.setValue(GlobalConstants.LoginString, forHTTPHeaderField: GlobalConstants.AuthorizarionField)
   request.httpMethod = httpMethod
   request.setValue("application/json", forHTTPHeaderField: "Content-Type")
   
   if (data != nil){
      request.httpBody = try? JSONSerialization.data(withJSONObject: data as Any, options: [])
      /*
         print("\n=====start body message: \n")
         print(NSString(data: request.httpBody!, encoding: String.Encoding.utf8.rawValue) as Any)
         print("\n====end body message: \n")
       */
   }
   
   HTTPsendRequest(request: request) {
      (data: String, error: String?) -> Void in
      
      if error != nil {
//         print("response error: " + "\(error)")
         callback(data as AnyObject, error)
      } else {
//         print("response data: " + "\(data)")
         let arrDict = JSONParsetoArray(jsonString: data)

         callback(arrDict as AnyObject, nil)
      }
   }
}


//parse JSON String to array
func JSONParsetoArray(jsonString:String) -> NSArray {
   
   if let data: NSData = jsonString.data(using: .utf8, allowLossyConversion: false) as NSData?{
      
      do{
         let jsonObj = try JSONSerialization.jsonObject(with: data as Data,options:.mutableLeaves)
         
         return jsonObj as! NSArray
         
      }catch{
         print("Error")
      }
   }
   return NSArray()
}
